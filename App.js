
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {Expense, Display, Category} from "./component/api/Expense";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { HomeScreen } from './components/HomeScreen';
import { DetailScreen } from './components/DetailScreen';
import { Login } from './components/LoginScreen';
import { ChartScreen } from './components/ChartScreen';


const Stack = createStackNavigator();

export function App() {
  return (
   
    <NavigationContainer>
    <Stack.Navigator>
      <Stack.Screen name="Expenses" component={HomeScreen} />
      <Stack.Screen name="Detail" component={DetailScreen} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Chart" component={ChartScreen} />
    </Stack.Navigator>
  </NavigationContainer>
  );
}

export default App;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        justifyContent: 'flex-start',
    },
});
