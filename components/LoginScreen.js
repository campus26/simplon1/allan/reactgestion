import * as React from 'react';
import { StyleSheet, View, Text, Button } from 'react-native';

import { TextInput } from 'react-native-paper';

export function Login ({ navigation }) {
  const [Mail, setMail] = React.useState('');
  const [Text, setText] = React.useState('');
  return (
      <View>
        <TextInput
         label="Email"
        value={Mail}
        onChangeText={Mail => setMail(Mail)}
        />
        <TextInput
         label="name"
        value={Text}
        onChangeText={Text => setText(Text)}
        />
        <Button icon="camera"  mode="contained"
        
        title="Submit"
        onPress={() => navigation.navigate('Expenses')}
        />
    </View>
  );
};

export default Login;

