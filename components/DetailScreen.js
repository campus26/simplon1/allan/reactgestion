
import React, { Component, useState, useEffect } from 'react';
import { TextInput, Button } from 'react-native-paper';
import { StyleSheet, Text, View, TouchableOpacity, Image, Alert, ScrollView, FlatList, Platform, SafeAreaView } from 'react-native';
import RNPickerSelect from "react-native-picker-select";
import DatePicker from 'react-native-datepicker';
import {Category} from "../component/api/Expense";


export function DetailScreen({ navigation }) {

    const [category, setSelect] = useState(null);
    const [name, setName] = useState("depense");
    const [amount, setAmount] = useState(1);

    const pickerStyle = {
        inputIOS: {
            color: 'white',
            paddingTop: 13,
            paddingHorizontal: 10,
            paddingBottom: 12,
        },
        inputAndroid: {
            color: 'white',
        },
        inputBrowser:{
            color: 'white',
            paddingTop: 13,
            paddingHorizontal: 10,
            paddingBottom: 12,
        },
        placeholderColor: 'white',
        underline: { borderTopWidth: 0 },
        icon: {
            position: 'absolute',
            backgroundColor: 'transparent',
            borderTopWidth: 5,
            borderTopColor: '#00000099',
            borderRightWidth: 5,
            borderRightColor: 'transparent',
            borderLeftWidth: 5,
            borderLeftColor: 'transparent',
            width: 0,
            height: 0,
            top: 20,
            right: 15,
        },
    };

    const [date, setDate] = useState(new Date())

    if(Platform.OS === 'ios' || Platform.OS === 'android'){
        return (
            <View style={{ flex: 1, justifyContent: 'center' }}>
                <View>
                    <TextInput
                        label="Name"
                        value={name}
                        onChangeText={name => setName(name)}
                    />
                    <TextInput
                        label="Amount"
                        value={String(amount)}
                        onChangeText={value => setAmount(value)}
                        keyboardType="numeric"
                    />
                    <RNPickerSelect
                        onValueChange={value => setSelect(value)}
                        style={pickerStyle}
                        items={Category()}
                    />

                    <SafeAreaView >
                        <View >

                            <DatePicker

                                date={date} //initial date from state
                                mode="date" //The enum of date, datetime and time
                                placeholder="select date"
                                format="YYYY-MM-DD"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                customStyles={{
                                    dateIcon: {
                                        //display: 'none',
                                        position: 'absolute',
                                        left: 0,
                                        top: 4,
                                        marginLeft: 0,
                                    },
                                    dateInput: {
                                        marginLeft: 36,
                                    },
                                }}
                                onDateChange={(date) => {
                                    setDate(date);
                                    console.log(date)
                                }}
                            />
                        </View>
                    </SafeAreaView>

                    <Button mode="outlined" onPress={() => AjoutExpense(name, amount, category, date,navigation)}>
                        Add Expense
                    </Button>
                </View>

                <Button onPress={() => navigation.navigate('Expenses')}>
                    Go to Expenses
                </Button>

                <Button onPress={() => navigation.navigate('Chart')}>
                    Go to Charts
                </Button>
            </View>
        );
    } else {
        return (
            <View style={{ flex: 1, justifyContent: 'center' }}>
                <View>
                    <TextInput
                        label="Name"
                        value={name}
                        onChangeText={name => setName(name)}
                    />
                    <TextInput
                        label="Amount"
                        value={String(amount)}
                        onChangeText={value => setAmount(value)}
                        keyboardType="numeric"
                    />
                    <RNPickerSelect
                        onValueChange={value => setSelect(value)}
                        items={Category()}
                        style={pickerStyle}
                    />
                    <input type="date" value={setDate}/>
                    <Button mode="outlined" onPress={() => AjoutExpense(name, amount, category, date,navigation)}>
                        Add Expense
                    </Button>
                </View>

                <Button onPress={() => navigation.navigate('Expenses')}>
                    Go to Expenses
                </Button>

            </View>
        );
    }

}

async function AjoutExpense(name, amount, category, date,navigation){

    const newExpense = {
        "date": date,
        "name": name,
        "amout": parseFloat(amount),
        "picture": "picture",
        "user" : 2,
        "category" : category
    }

    console.log(name);
    console.log(amount);

    await fetch('http://192.168.1.155/apigestion/public/newexpense', {
        method: 'POST',
        body: JSON.stringify(newExpense)
    });
console.log(navigation);
    navigation.navigate('Expenses')
}


const yes = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f5f',
        justifyContent: 'center',
    },
});
